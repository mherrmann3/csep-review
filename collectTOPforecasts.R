# Searches for the highest forecast rates
#    (in all output files for a given experiment)


collectTOPforecasts <- function(Region,TOP,minMag) {
  
#   Regions = c("CA-one-day-models", # Mmin = 3.95
#                   "GLOBE-one-day-models", # Mmin = 5.95
#                   "NWPacific-one-day-models", # Mmin = 5.8
#                   "SWPacific-one-day-models") # Mmin = 5.8
#   Region = Regions[3]
#   TOP = 1000
#   minMag = 5.95

  # Load required functions
  source('getSpatialForecast.R')
  

  # How is the region called? - In case someone throws in an string with the experiment ("-VXX.XX")
  Region = unlist(strsplit(Region,"-V"))[1]

  # List all experiments of this region
  listExperiments = dir(paste0("~/CSEPdata/"))
  listExperiments = listExperiments[grep(Region,listExperiments)]
  firstExp = listExperiments[1]
  lastExp = tail(listExperiments,1)

  # Get available dates
  allDateFolders=dir(paste0("~/CSEPdata/",firstExp,"/results"))
  dateStart = as.Date(allDateFolders[1],format = "%Y-%m-%d")
  lastDateFolderIndex = tail(grep("[0-9]{4}-[0-9]{2}-[0-9]{2}",allDateFolders),1)
  dateEnd = as.Date(allDateFolders[lastDateFolderIndex],format = "%Y-%m-%d")
  # Create Date vector
  Dates = seq(dateStart,to = dateEnd, by='days')
  
  # Get available models 
  # ... using old school param-file method
  Models = as.matrix(read.table(file=paste0("param/",lastExp,".param"), header = FALSE))
    
  # An array which will contain all dates where SOMETHING is missing
  missingDates = structure(numeric(0), class = "Date")
  
                          
   # Define temporary array for storage (sry, MATLAB habits)
  TOPforecasts = data.frame(as.Date(character()),
                            Model = character(),
                            matrix(vector(), 0, 4),
                            CumRate = numeric())
  
  print(paste0("having ", length(Dates), " days"), quote = FALSE)
  # loop over dates
  for (iDay in seq_along(Dates)) {
    print(paste(iDay, ":", Dates[iDay]))
    if (iDay %% 100 == 0) cat(paste0(iDay,"\n"))
    errorCnt = 0
    
    # loop over models
    for (iModel in seq_along(Models)) {
      
      possibleError = tryCatch({
        
        spaForecstNow = getSpatialForecast(firstExp, Dates[iDay], Models[iModel], minMag)
        
      }, error = function(e) e)
      
      # If error,... (ignoring the STEP turn-off)
      if(inherits(possibleError, "error") && Models[iModel]!="STEP") {
        missingDates[length(missingDates)+1] = Dates[iDay]
#         if (errorCnt == 0) {cat("X"); errorCnt = 1} # Shows an X when value has not been found
#         cat("\n"); print(paste0("Something Missing [", Dates[iDay],"] ",possibleError))
        # Continue with the next model
        next
      }

    # Get the new TOP XXX (by sorting)
    tmpList = rbind(TOPforecasts,spaForecstNow)
    tmpList = tmpList[!is.na(tmpList$CumRate),] # b/c sorting NA values is a problem, when returning indexes
    TOPforecasts = tmpList[sort(tmpList$CumRate, decreasing = TRUE, index.return=TRUE)$ix[1:TOP],]

    }
    if (errorCnt == 0) cat(".")
  }
  
  # Remove duplicate dates
  missingDates = unique(missingDates)
  
  # ADDITIONAL INFO
  # 1. Get the most recent experiment indices
  startingDates = as.Date(paste0(unlist(
    strsplit(listExperiments,"-V"))[2*(1:length(listExperiments))]
    ,".01"),"%y.%m.%d")
  ExperimentIndex = sapply(as.Date(TOPforecasts$Date),function(x) max(which(x>=startingDates)))

  #   TOPforecasts = read.table("data/TOP1000_M3.95+_CA-one-day-models.dat")[,-2] #DEBUG
  # Read catalog file
  catalogs=dir("catalogs")
  catalog = read.fwf(paste0("catalogs/",catalogs[grep(strsplit(Region,"-")[[1]][1],catalogs)]),
                     widths=c(11,12,9,10,7,6,6,4,4,5,5,4,12), skip=12)
  
  # 2. How many EQ's actually happenend?
  NumEQobs = NumEQobs9 = array(0, dim=TOP)
  spaGridding=diff(as.numeric(TOPforecasts[1,3:4]))
  for (iTOP in 1:TOP) {
    # (ignoring the depth, but the catalog has 0<z<30km anyway).
    onDay=as.Date(catalog[,1])==as.Date(TOPforecasts$Date[iTOP])
    inLon=catalog[,4]>=TOPforecasts$V1[iTOP]&catalog[,4]<TOPforecasts$V2[iTOP]
    inLat=catalog[,3]>=TOPforecasts$V3[iTOP]&catalog[,3]<TOPforecasts$V4[iTOP]
    inMag=catalog[,6]>=minMag # This could be quite critical, but the catalogs should not contain lower magnitudes than used in the experiments. Relax.
    NumEQobs[iTOP] = length(which(onDay&inLon&inLat&inMag))
    # Be gracious (aka: did the EQ happened in the "surrounding" bins?)
    inLon9=catalog[,4]>=TOPforecasts$V1[iTOP]-spaGridding&catalog[,4]<TOPforecasts$V2[iTOP]+spaGridding
    inLat9=catalog[,3]>=TOPforecasts$V3[iTOP]-spaGridding&catalog[,3]<TOPforecasts$V4[iTOP]+spaGridding
    NumEQobs9[iTOP] = length(which(onDay&inLon9&inLat9&inMag))
  }  

  # ... and put everything in the dataframe
  TOPforecasts = cbind(Date=TOPforecasts[,1], ExperimentNr=ExperimentIndex,TOPforecasts[,-1],ObsEQ=NumEQobs,ObsEQ9=NumEQobs9)
  rownames(TOPforecasts) = c(1:TOP)
  
  # Output THE TOP table
  filename=paste0("data/TOP-spatialBins/",Region,"_M",minMag,"+_","TOP",as.integer(TOP))
  write.table(TOPforecasts, file = paste0(filename,".dat"), quote = FALSE)
  write.table(missingDates, file = paste0(filename,"_missingDates.dat"), quote = FALSE)
  
  
  # PLOTTING 
  # 1. distribution among the experiments
#   png(file = paste0(filename,"_ExpNr-hist.png"),width=500,height=500)
#   hist(ExperimentIndex, breaks=sort(unique(ExperimentIndex))+0.5, xaxt="n", col="grey90",xlab="", main=paste0("Histogram of TOP",TOP))
#   axis(1,at=sort(unique(ExperimentIndex)), labels=listExperiments[sort(unique(ExperimentIndex))], las=2, hadj = -1.2, tick = FALSE)
#   dev.off()
  
}
