# For a given experiment and test, it retrieves the test results
#     (by munching through all the dates)
# Saves everything in tables with model in filename (structured in separate folders by tests)
# N-Test/CumRate: Grabs forecast rate ("eventCountForecast") and observed rate ("eventCount")
# L-Test: Grabs "logLikelihoodTrue"-sum, if available
# M-Test
# S-Test
# T-Test: Grabs "meanInformationGain" and "upper/lowerConfidenceLimit"

collectDataFromResults <- function(Experiment,Test) {
  
  # Load required functions
  require('XML')
  source('getFromDailyTestFile.R')
  source('getReorderedValues.R')
  
  
#   Experiment = "CA-one-day-models-V07.08"
#   Test = "T-Test"
#   Model = "ETAS"
  
  # Read init XML
  dirExperiment = paste0("~/CSEPdata/",Experiment)
#   initXML = paste0("~/CSEPdata/",Experiment,"/forecast.init.xml")
  allFolders=dir(paste0(dirExperiment,"/results"))
  
  # Get available dates

#   dateStart <- as.Date(xmlToList(xmlParse(initXML))[["entryDate"]],format = "%Y-%m-%d")
  dateStart <- as.Date(allFolders[1],format = "%Y-%m-%d")
#   dateEnd = as.Date('2014-03-31',format = "%Y-%m-%d")
  lastDateFolderIndex = tail(grep("[0-9]{4}-[0-9]{2}-[0-9]{2}",allFolders),1)
  dateEnd = as.Date(allFolders[lastDateFolderIndex],format = "%Y-%m-%d")
  # Create Date vector
  Dates = seq(dateStart,to = dateEnd, by='days')
  
  # Get available models 
  # ... reading the init-XML (sadly, XML file does not contain all "proper" model names :-(
  # Models = scan(text=gsub("\n","", xmlToList(xmlParse(initXML))[["models"]][["text"]]),what="",sep=" ")
  # Models = c(Models[Models != ""],"STEP") # Clean array & add STEP (which is always missing)
  # ... using old school param-file method
  Models = as.matrix(read.table(file=paste0("param/",Experiment,".param"), header = FALSE))
  
  if (Test=="T-Test" && length(Models)==1) {
    warning("T-Test doesn't make sense for this experiment")
    return(1)
  }

  # An array which will contain all dates where SOMETHING is missing
  missingDates = structure(numeric(0), class = "Date")
  
  # Experimental, may facilitate scripting
#   TestParams = read.table(paste0("param/Test_",Test,".param"))
                          
  if (Test=="T-Test") {
    dim = 2
  } else
    dim = 1
  # Define temporary array for storage (sry, MATLAB habits)
  readValue1 = array(NA, dim=c(length(Dates),length(Models)^dim))
  readValue1low = readValue1
  readValue1upp = readValue1
  readValue1other = readValue1
  readValue2 = array(NA, dim=c(length(Dates),1))
  readValuePval1 = array(NA, dim=c(length(Dates),length(Models)))
  readValuePval2 = array(NA, dim=c(length(Dates),length(Models)))
  
  print(paste0("having ", length(Dates), " days"), quote = FALSE)
  # loop over dates
  for (iDay in seq_along(Dates)) {
#     print(paste(iDay, ":", Dates[iDay]))
    if (iDay %% 100 == 0) cat(paste0(iDay,"\n"))
    errorCnt = 0
    
    # loop over models
    for (iModel in seq_along(Models)) {
      
      possibleError = tryCatch({
        # Reading values concering N-Test
        if (Test=="N-Test") {
          # Get observed rate (only read true rates once)
          if (iModel==1 && Models[iModel]!="STEP" || iModel==2) {
            readValue2[iDay] = as.numeric(
                    getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"eventCount"))
          }
          # Get forecast rate
          readValue1[iDay,iModel] = as.numeric(
                  getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"eventCountForecast"))
          # Get the p values
          readValuePval1[iDay,iModel] = as.numeric(
                  getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"delta1"))
          readValuePval2[iDay,iModel] = as.numeric(
                  getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"delta2"))
        }
        else if (Test=="L-Test") {
          # Get LogLikelihood
          readValue1[iDay,iModel] = as.numeric(
                  getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],c("logLikelihoodTrue","sum")))
          # Get the p values
          readValuePval1[iDay,iModel] = as.numeric(
                  getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"gamma"))
        }
        else if (Test=="M-Test") {
          # Get the p values
          readValuePval1[iDay,iModel] = as.numeric(
            getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"kappa"))
        }
        else if (Test=="S-Test") {
          # Get LogLikelihood
          readValue1[iDay,iModel] = as.numeric(
            getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],c("logLikelihoodTrue","sum")))
          # Get the p value
          readValuePval1[iDay,iModel] = as.numeric(
            getFromDailyTestFile(Experiment,Dates[iDay],Test,Models[iModel],"zeta"))
        } 
        #TODO: make for the case if NOTHING IS MISSING!
        else if (Test=="T-Test" && length(Models)>1) { # Do only if T-Test makes sense
          if (iModel==2) { # ignore flawed STEP; + two models have to be present anyway
            # Retrieve order of the models + and their presence
            ModelOrder = getFromDailyTestFile(Experiment,Dates[iDay],Test,NULL,"name")
            # Bring model names in great shape
            ModelsNow = sapply(ModelOrder, function(x) unlist(strsplit(x,"_[0-9]{1,2}_"))[1], USE.NAMES=FALSE)
            # Get RoundRobin-style information gain
            readArray = array(as.numeric(
                              getFromDailyTestFile(Experiment,Dates[iDay],Test,NULL,"meanInformationGain")),
                              c(length(ModelsNow),length(ModelsNow)))
            readValue1[iDay,] = as.numeric(getReorderedValues(readArray,ModelsNow,Models))
            # lower confidence bound
            readArrayLow = array(as.numeric(
                              getFromDailyTestFile(Experiment,Dates[iDay],Test,NULL,"lowerConfidenceLimits")),
                              c(length(ModelsNow),length(ModelsNow)))
            readValue1low[iDay,] = as.numeric(getReorderedValues(readArrayLow,ModelsNow,Models))
            # upper confidence bound
            readArrayUpp = array(as.numeric(
                              getFromDailyTestFile(Experiment,Dates[iDay],Test,NULL,"upperConfidenceLimits")),
                              c(length(ModelsNow),length(ModelsNow)))
            readValue1upp[iDay,] = as.numeric(getReorderedValues(readArrayUpp,ModelsNow,Models))
            # num of events
            readArrayOth = array(as.numeric(
                            getFromDailyTestFile(Experiment,Dates[iDay],Test,NULL,"numberEvents")),
                            c(length(ModelsNow),length(ModelsNow)))
            readValue1other[iDay,] = as.numeric(getReorderedValues(readArrayOth,ModelsNow,Models))
          }
        } 
      }, error = function(e) e)
      
      # If error,... (but ignoring the STEP turn-off)
      if(inherits(possibleError, "error") && Models[iModel]!="STEP") {
        # Save the date in a vector (only if error AT THIS DATE not yet occurred)
        # if (missingDates1[length(missingDates1)+1] != Dates[iDay])
        missingDates[length(missingDates)+1] = Dates[iDay]
        if (errorCnt == 0) {cat("X"); errorCnt = 1} # Shows an X when value has not been found
#         cat("\n"); print(paste0("Something Missing [", Dates[iDay],"] ",possibleError))
        # Continue with the next model
        next
      }
      
    }
    if (errorCnt == 0) cat(".")
  }
  
  # Remove duplicate dates
  missingDates = unique(missingDates)
  
  # Generate tables
  Values1 = data.frame(Dates,readValue1)
  Values1low = data.frame(Dates,readValue1low)
  Values1upp = data.frame(Dates,readValue1upp)
  Values1other = data.frame(Dates,readValue1other)
  Values2 = data.frame(Dates,readValue2)
  ValuesPval1 = data.frame(Dates,readValuePval1)
  ValuesPval2= data.frame(Dates,readValuePval2)
  # Output tables
  if (Test=="N-Test") {
      write.table(Values2, file = paste0("data/",Experiment,"/",Test,"_RateObs.dat"), quote = FALSE)
      write.table(Values1, file = paste0("data/",Experiment,"/",Test,"_RateForecst.dat"), quote = FALSE)
      write.table(ValuesPval1, file = paste0("data/",Experiment,"/",Test,"_delta1.dat"), quote = FALSE)
      write.table(ValuesPval2, file = paste0("data/",Experiment,"/",Test,"_delta2.dat"), quote = FALSE)
  } else if (Test=="L-Test") {
      write.table(Values1, file = paste0("data/",Experiment,"/",Test,"_jointLogLikelihood.dat"), quote = FALSE)
      write.table(ValuesPval1, file = paste0("data/",Experiment,"/",Test,"_gamma.dat"), quote = FALSE)
  } else if (Test=="M-Test") {
    write.table(ValuesPval1, file = paste0("data/",Experiment,"/",Test,"_kappa.dat"), quote = FALSE)
  } else if (Test=="S-Test") {
    write.table(Values1, file = paste0("data/",Experiment,"/",Test,"_jointLogLikelihood.dat"), quote = FALSE)
    write.table(ValuesPval1, file = paste0("data/",Experiment,"/",Test,"_zeta.dat"), quote = FALSE)
  } else if (Test=="T-Test" && length(Models)>1) {
    write.table(Values1, file = paste0("data/",Experiment,"/",Test,"_meanInformationGain.dat"), quote = FALSE)
    write.table(Values1low, file = paste0("data/",Experiment,"/",Test,"_lowerConfidenceLimit.dat"), quote = FALSE)
    write.table(Values1upp, file = paste0("data/",Experiment,"/",Test,"_upperConfidenceLimit.dat"), quote = FALSE)
    write.table(Values1other, file = paste0("data/",Experiment,"/",Test,"_numberEvents.dat"), quote = FALSE)
  }
  write.table(missingDates, file = paste0("data/",Experiment,"/",Test,"_missingDates.dat"), quote = FALSE)

}
