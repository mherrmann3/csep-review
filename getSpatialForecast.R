# For a given firstExp, model, date, and minimum magnitude
# returns the spatial rate forecast

getSpatialForecast <- function(firstExp, Date, Model, minMag) {
#   firstExp = "CA-one-day-models-V07.08"
#   firstExp = "GLOBE-one-day-models-V09.07"
#   Date = as.Date("2009-12-03", "%Y-%m-%d")
#   Model = "STEP"
#   Model = "KJSSGlobalOneDay"
#   minMag = 6
 
  folder = paste0("~/CSEPdata/", firstExp, "/forecasts/archive/", format(Date, "%Y_%-m"),"/")
  
  filePattern = paste0(
    folder,
    Model,"_",
    format(Date, "%-m_%-d_%Y"),
    "-fromXML.dat")
  
  # Read in forecast
  if (file.exists(filePattern)) {
    forecastTable = as.matrix(read.table(file = filePattern, na.strings = "NA", colClasses="numeric"))
  } else {
    return('NoFile')
  }
  
  
  # TODO: using mat-file to speed up (smaller file size)
#   require("R.matlab")
#   forecastTable = as.matrix(readMat(file = filePattern)$mModel)
  
  # Get number of magnitude bins (searches for the first entry, which differs in the spatial bin limits
  # (IMPORTANT: could deliver wrong values, but works fine SO FAR) - mind the "102"
  numMagBins = length(which(apply(forecastTable[1:102,1:4],1,'==',forecastTable[1,1:4])[4,]))
  # Get number of magnitude bins
  numSpaBins = nrow(forecastTable)/numMagBins

  # Get the cumulative rates in a SPATIAL bin
  if (numMagBins==1) {
    CumRates = forecastTable[,9]
  } else {
    # The first magnitude bin of interest
    minMagBinIndex = which(forecastTable[1:numMagBins,7]>=minMag)[1]
    # The approach (reshaping [array], truncating [X1:end], sum [apply to every spa-bin])
    CumRates = apply(
      array(forecastTable[,9], c(numMagBins,numSpaBins))[minMagBinIndex:numMagBins,],
      2,sum) # switched rows+columns due to column-fill-preference
  }
  
  # Create output matrix (Date | Model | Spatial bins [x4] | CumRate) 
  spaForecast = data.frame(Date = rep(as.Date(Date, "%Y-%m-%d"),numSpaBins),
                      Model = as.character(rep(Model,numSpaBins)),
                      forecastTable[seq(1, numSpaBins*numMagBins, numMagBins),1:4],
                      CumRate=CumRates)
  colnames(spaForecast)[3:6] = c("x1", "x2", "y1", "y2")
  
 # DEBUG
#   sum(forecastTable[36:51,9])

  return(spaForecast)
    
}