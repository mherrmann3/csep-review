#!/bin/bash

# avi movie
#avconv -i "$1"/*_%04d.png -r 10 -q 0 $1''_10fps.avi
#ffmpeg -i "$1"/*_%04d.png -r 10 -qscale 1 $1''_10fps_v2.avi

cd $1
mencoder "mf://*.png" -mf fps=10 -o ../$1''_10fps.avi  -ovc lavc -lavcopts vcodec=mpeg4:vqscale=1 # -ovc raw 
cd ..

echo "Doing .GIF"
convert -delay 10 -loop 0 "$1"/*.png $1''_10fps.gif

